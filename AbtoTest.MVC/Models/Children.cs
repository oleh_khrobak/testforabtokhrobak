﻿using AbtoTest.MVC.Models.Abstractions;
using System.Data.SqlClient;

namespace AbtoTest.MVC.Models;

public sealed class Children : Entity
{
    public Children(
        Guid id,
        Guid sponsorId,
        string childNumber, 
        string firstName, 
        string lastName, 
        DateTime birthDate, 
        double pledgeAmount, 
        string photo) 
        : base(id)
    {
        SponsorId = sponsorId;
        ChildNumber = childNumber;
        FirstName = firstName;
        LastName = lastName;
        BirthDate = birthDate;
        PledgeAmount = pledgeAmount;
        Photo = photo;
    }

    public Children()
    {
    }

    public Guid SponsorId { get; set; }

    public string ChildNumber { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public DateTime BirthDate { get; set; }

    public double PledgeAmount { get; set; }

    public string Photo { get; set; }

    public Sponsor? Sponsor { get; set; }
}
