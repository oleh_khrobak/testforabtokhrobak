﻿using AbtoTest.MVC.Models.Abstractions;

namespace AbtoTest.MVC.Models;

public sealed class Sponsor : Entity
{
    public Sponsor(
        Guid id, 
        string firstName, 
        string lastName, 
        string email, 
        DateTime birthDate, 
        string note, 
        List<Children> children) 
        : base(id)
    {
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        BirthDate = birthDate;
        Note = note;
        Children = children;
    }

    public Sponsor()
    {
    }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Email { get; set; }

    public DateTime BirthDate { get; set; }

    public string Note { get; set; }

    public List<Children> Children { get; set; }
}
