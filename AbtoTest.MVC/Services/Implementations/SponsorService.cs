﻿using AbtoTest.MVC.Infrastructure.Repositories.Interfaces;
using AbtoTest.MVC.Models;
using AbtoTest.MVC.Services.Interfaces;
using AbtoTest.MVC.Shared;

namespace AbtoTest.MVC.Services.Implementations;

public sealed class SponsorService : ISponsorService
{
    private readonly ISponsorRepository _sponsorRepository;

    public SponsorService(ISponsorRepository sponsorRepository)
    {
        _sponsorRepository = sponsorRepository;
    }

    public Result<Sponsor> GetById(Guid id)
    {
        var sponsor = _sponsorRepository.GetById(id);

        return sponsor ?? Result.Failure<Sponsor>(Error.Sponsor.NotFound);
    }

    public Result Update(Guid id,
        string firstName,
        string lastName,
        string email,
        DateTime birthDate,
        string note)
    {
        var sponsor = _sponsorRepository.GetById(id);

        if (sponsor is null)
        {
            return Result.Failure(Error.Sponsor.NotFound);
        }

        _sponsorRepository.Update(id, firstName, lastName, email, birthDate, note);

        return Result.Success();
    }
}
