﻿using AbtoTest.MVC.Models;

namespace AbtoTest.MVC.Services.Interfaces;

public interface ISponsorService
{
    Result<Sponsor> GetById(Guid id);

    Result Update(Guid id,
        string firstName,
        string lastName,
        string email,
        DateTime birthDate,
        string note);
}
