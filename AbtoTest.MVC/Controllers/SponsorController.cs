﻿using AbtoTest.MVC.Models;
using AbtoTest.MVC.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace AbtoTest.MVC.Controllers;
public class SponsorController : Controller
{
    private readonly ISponsorService _sponsorService;

    public SponsorController(ISponsorService sponsorService)
    {
        _sponsorService = sponsorService;
    }

    [HttpGet]
    public IActionResult Update(Guid id)
    {
        
        Result<Sponsor> result = _sponsorService.GetById(id);

        if (result.IsFailure)
        {
            return NotFound();
        }

        return View(result.Value);
    }

    [HttpPost]
    public IActionResult Update(Sponsor sponsor)
    {
        Result result = _sponsorService.Update(
            sponsor.Id,
            sponsor.FirstName,
            sponsor.LastName,
            sponsor.Email,
            sponsor.BirthDate,
            sponsor.Note);

        if (result.IsFailure)
        {
            return NotFound();
        }

        return View(sponsor);
    }
}
