using AbtoTest.MVC.Hubs;
using AbtoTest.MVC.Infrastructure.Repositories.Implementations;
using AbtoTest.MVC.Infrastructure.Repositories.Interfaces;
using AbtoTest.MVC.Services.Implementations;
using AbtoTest.MVC.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddScoped<ISponsorService, SponsorService>();
builder.Services.AddScoped<ISponsorRepository, SponsorRepository>();

builder.Services.AddSignalR();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.UseEndpoints(endpoints =>
{
    endpoints.MapHub<UpdateHub>("/update");
});

app.Run();
