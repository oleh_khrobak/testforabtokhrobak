﻿namespace AbtoTest.MVC.Shared;

public class Error
{
    public static readonly Error None = new Error(string.Empty);

    public Error(string message)
    {
        Message = message;
    }

    public string Message { get; }

    public static class Sponsor
    {
        public static Error NotFound => new("Sponsor with provided id was not found");
    }
}
