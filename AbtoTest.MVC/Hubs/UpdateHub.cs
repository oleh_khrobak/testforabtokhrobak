﻿using AbtoTest.MVC.Models;
using Microsoft.AspNetCore.SignalR;

namespace AbtoTest.MVC.Hubs;

public class UpdateHub : Hub
{
    public async Task Notify(string data)
    {
        await this.Clients.All.SendAsync("Notify", data);
    }
}
