﻿using AbtoTest.MVC.Models;

namespace AbtoTest.MVC.Infrastructure.Repositories.Interfaces;

public interface ISponsorRepository
{
    Sponsor? GetById(Guid id);

    void Update(
        Guid id, 
        string firstName, 
        string lastName, 
        string email, 
        DateTime birthDate, 
        string note);
}
