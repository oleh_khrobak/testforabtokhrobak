﻿using AbtoTest.MVC.Infrastructure.Repositories.Interfaces;
using System.Data.SqlClient;
using AbtoTest.MVC.Models;

namespace AbtoTest.MVC.Infrastructure.Repositories.Implementations;

public sealed class SponsorRepository : ISponsorRepository
{
    private readonly string? _connectionString;

    public SponsorRepository(IConfiguration configuration)
    {
        _connectionString = configuration.GetConnectionString("sqlConnection");
    }

    public Sponsor? GetById(Guid id)
    {
        using SqlConnection connection = new SqlConnection(_connectionString);
        connection.Open();

        const string sql = @"SELECT s.Id, s.FirstName, s.LastName, s.Email, s.BirthDate, s.Note,
                             c.Id AS ChildId, c.ChildNumber, c.FirstName AS ChildFirstName, 
                             c.LastName AS ChildLastName, c.BirthDate as ChildBirthDate, c.PledgeAmount, c.Photo 
                             FROM Sponsors AS s
                             LEFT JOIN Children c ON s.Id = c.SponsorId
                             WHERE s.Id = @id";

        SqlCommand command = new SqlCommand(sql, connection);
        SqlParameter idParam = new SqlParameter("@id", id);

        command.Parameters.Add(idParam);

        Sponsor? sponsor = null;

        using SqlDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
            if (sponsor == null)
            {
                string firstName = reader.GetString(reader.GetOrdinal("FirstName"));
                string lastName = reader.GetString(reader.GetOrdinal("LastName"));
                string email = reader.GetString(reader.GetOrdinal("Email"));
                DateTime birthDate = reader.GetDateTime(reader.GetOrdinal("BirthDate"));
                string note = reader.GetString(reader.GetOrdinal("Note"));


                sponsor = new Sponsor(
                    id,
                    firstName,
                    lastName,
                    email,
                    birthDate,
                    note,
                    new List<Children>());
            }

            if (!reader.IsDBNull(reader.GetOrdinal("ChildId")))
            {
                Guid childId = reader.GetGuid(reader.GetOrdinal("ChildId"));
                string childNumber = reader.GetString(reader.GetOrdinal("ChildNumber"));
                string firstName = reader.GetString(reader.GetOrdinal("ChildFirstName"));
                string lastName = reader.GetString(reader.GetOrdinal("ChildLastName"));
                DateTime birthDate = reader.GetDateTime(reader.GetOrdinal("ChildBirthDate"));
                double pledgeAmount = reader.GetDouble(reader.GetOrdinal("PledgeAmount"));
                string photo = reader.GetString(reader.GetOrdinal("Photo"));

                sponsor.Children.Add(
                    new Children(
                        childId,
                        id,
                        childNumber,
                        firstName,
                        lastName,
                        birthDate,
                        pledgeAmount,
                        photo));
            }
        }

        return sponsor;
    }

    public void Update(
        Guid id, 
        string firstName, 
        string lastName, 
        string email, 
        DateTime birthDate, 
        string note)
    {
        using SqlConnection connection = new SqlConnection(_connectionString);
        connection.Open();

        const string sql = @"UPDATE Sponsors
                                     SET FirstName = @FirstName,
                                         LastName = @LastName,
                                         Email = @Email,
                                         BirthDate = @BirthDate,
                                         Note = @Note
                                     WHERE Id = @Id";

        SqlCommand command = new SqlCommand(sql, connection);
        command.Parameters.AddWithValue("@FirstName", firstName);
        command.Parameters.AddWithValue("@LastName", lastName);
        command.Parameters.AddWithValue("@Email", email);
        command.Parameters.AddWithValue("@BirthDate", birthDate);
        command.Parameters.AddWithValue("@Note", note);
        command.Parameters.AddWithValue("@Id", id);

        command.ExecuteNonQuery();
    }
}
