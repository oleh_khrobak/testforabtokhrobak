INSERT INTO Sponsors (ID, FirstName, LastName, Email, BirthDate, Note)
VALUES 
    ('a1e7b702-9b67-4a8d-bdc7-6d0bf8500e19', 'John', 'Doe', 'john.doe@example.com', '1990-05-15', NULL),
    ('c06a3c9d-646d-4b30-b9c1-f1b4a17dc079', 'Jane', 'Smith', 'jane.smith@example.com', '1985-08-20', NULL);

INSERT INTO Children (ID, SponsorId, ChildNumber, FirstName, LastName, BirthDate, PledgeAmount, Photo)
VALUES 
    ('c1e0523a-fd78-40a0-ba27-619b5469bc33', 'a1e7b702-9b67-4a8d-bdc7-6d0bf8500e19', '001', 'Alice', 'Doe', '2010-03-10', 100, 'photo1.jpg'),
    ('7ecf122a-302e-4b2d-b5f2-432660693b62', 'a1e7b702-9b67-4a8d-bdc7-6d0bf8500e19', '002', 'Bob', 'Doe', '2012-07-25', 150, 'photo2.jpg'),
    ('3bc2e318-18dc-4b38-9d04-32d7d5c0ef8f', 'c06a3c9d-646d-4b30-b9c1-f1b4a17dc079', '001', 'Emily', 'Smith', '2009-11-18', 120, 'photo3.jpg');
